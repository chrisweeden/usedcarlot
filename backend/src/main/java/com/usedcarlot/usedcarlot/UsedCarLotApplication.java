package com.usedcarlot.usedcarlot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsedCarLotApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsedCarLotApplication.class, args);
	}

}
