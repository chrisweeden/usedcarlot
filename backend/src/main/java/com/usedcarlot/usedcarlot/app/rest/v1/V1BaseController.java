package com.usedcarlot.usedcarlot.app.rest.v1;

import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/v1")
public abstract class V1BaseController { }
