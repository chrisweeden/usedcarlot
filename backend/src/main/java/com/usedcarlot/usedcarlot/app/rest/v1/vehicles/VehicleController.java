package com.usedcarlot.usedcarlot.app.rest.v1.vehicles;

import com.usedcarlot.usedcarlot.app.rest.v1.V1BaseController;
import com.usedcarlot.usedcarlot.core.vehicles.VehicleService;
import com.usedcarlot.usedcarlot.core.vehicles.commands.CreateVehicle;
import com.usedcarlot.usedcarlot.core.vehicles.commands.UpdateVehicle;
import com.usedcarlot.usedcarlot.core.vehicles.dto.VehicleDto;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

@RestController
public class VehicleController extends V1BaseController {
    private final VehicleService service;
    private final VehicleModelAssembler assembler;

    public VehicleController(VehicleService service, VehicleModelAssembler assembler) {
        this.service = service;
        this.assembler = assembler;
    }

    @GetMapping("/vehicles")
    @ResponseStatus(HttpStatus.OK)
    public CollectionModel<EntityModel<VehicleDto>> index() {
        List<EntityModel<VehicleDto>> response = service.listVehicles().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(response);
    }

    @PostMapping("/vehicles")
    @ResponseStatus(HttpStatus.OK)
    public EntityModel<VehicleDto> create(@RequestBody @Valid VehicleRequest request,
                                          BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {}

        final VehicleDto vehicle = service.createVehicle(new CreateVehicle());
        return assembler.toModel(vehicle);
    }

    @GetMapping("/vehicles/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EntityModel<VehicleDto> show(@PathVariable Long id) {
        final VehicleDto vehicle = service.getVehicle(id);
        return assembler.toModel(vehicle);
    }

    @PutMapping("/vehicles/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EntityModel<VehicleDto> update(@PathVariable Long id,
                                          @RequestBody @Valid VehicleRequest request,
                                          BindingResult bindingResult) {
        final VehicleDto vehicle = service.updateVehicle(id, new UpdateVehicle());
        return assembler.toModel(vehicle);
    }

    @DeleteMapping("/vehicles/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id) {
        service.deleteVehicle(id);
    }
}
