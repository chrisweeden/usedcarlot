package com.usedcarlot.usedcarlot.app.rest.v1.vehicles;

import com.usedcarlot.usedcarlot.core.vehicles.dto.VehicleDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class VehicleModelAssembler implements RepresentationModelAssembler<VehicleDto, EntityModel<VehicleDto>> {
    @Override
    public EntityModel<VehicleDto> toModel(final VehicleDto model) {
        return EntityModel.of(
            model,
            linkTo(methodOn(VehicleController.class).show(model.getId())).withSelfRel(),
            linkTo(methodOn(VehicleController.class).index()).withRel("vehicles")
        );
    }
}
