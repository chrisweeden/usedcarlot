package com.usedcarlot.usedcarlot.configuration;

import com.usedcarlot.usedcarlot.core.vehicles.VehicleRepository;
import com.usedcarlot.usedcarlot.core.vehicles.domain.Vehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(VehicleRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new Vehicle("Ford", "F150", "2020")));
            log.info("Preloading " + repository.save(new Vehicle("Chevrolet", "Silverado", "2020")));
            log.info("Preloading " + repository.save(new Vehicle("Nissan", "Titan", "2020")));
        };
    }
}
