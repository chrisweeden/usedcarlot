package com.usedcarlot.usedcarlot.core.vehicles;

public class VehicleNotFoundException extends RuntimeException {
    public VehicleNotFoundException(Long id) {
        super(String.format("Could not find vehicle %d", id));
    }
}
