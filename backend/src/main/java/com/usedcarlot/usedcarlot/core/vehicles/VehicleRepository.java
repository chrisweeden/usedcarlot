package com.usedcarlot.usedcarlot.core.vehicles;

import com.usedcarlot.usedcarlot.core.vehicles.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
}
