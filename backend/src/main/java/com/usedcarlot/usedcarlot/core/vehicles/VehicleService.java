package com.usedcarlot.usedcarlot.core.vehicles;

import com.sun.istack.NotNull;
import com.usedcarlot.usedcarlot.core.vehicles.commands.CreateVehicle;
import com.usedcarlot.usedcarlot.core.vehicles.commands.UpdateVehicle;
import com.usedcarlot.usedcarlot.core.vehicles.domain.Vehicle;
import com.usedcarlot.usedcarlot.core.vehicles.dto.VehicleDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VehicleService {
    private VehicleRepository repository;

    public VehicleService(VehicleRepository repository) {
        this.repository = repository;
    }

    public List<VehicleDto> listVehicles() {
        return repository.findAll().stream()
                .map(VehicleDto::from)
                .collect(Collectors.toList());
    }

    public List<VehicleDto> listVehicles(final Pageable pageable) {
        final Page<Vehicle> vehicles = repository.findAll(pageable);
        return new ArrayList<>();
    }

    public VehicleDto getVehicle(@NotNull final Long id) {
        return VehicleDto.from(loadVehicle(id));
    }

    public VehicleDto createVehicle(@NotNull final CreateVehicle cmd) {
        final Vehicle vehicle = new Vehicle();
        vehicle.setMake(cmd.getMake());
        vehicle.setModel(cmd.getModel());
        vehicle.setModelYear(cmd.getModelYear());

        final Vehicle savedVehicle = repository.save(vehicle);

        return VehicleDto.from(savedVehicle);
    }

    public VehicleDto updateVehicle(@NotNull final Long id, @NotNull final UpdateVehicle cmd) {
        final Vehicle vehicle = loadVehicle(id);
        vehicle.setMake(cmd.getMake());
        vehicle.setModel(cmd.getModel());
        vehicle.setModelYear(cmd.getModelYear());

        final Vehicle savedVehicle = repository.save(vehicle);

        return VehicleDto.from(savedVehicle);
    }

    public void deleteVehicle(@NotNull final Long id) {
        final Vehicle vehicle = loadVehicle(id);

        repository.delete(vehicle);
    }

    private Vehicle loadVehicle(@NotNull final Long id) {
        return repository.findById(id).orElseThrow(() -> new VehicleNotFoundException(id));
    }
}
