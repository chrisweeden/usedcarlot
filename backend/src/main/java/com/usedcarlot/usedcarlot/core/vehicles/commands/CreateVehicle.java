package com.usedcarlot.usedcarlot.core.vehicles.commands;

public class CreateVehicle {
    private String make;

    private String model;

    private String modelYear;

    public String getMake() {
        return make;
    }

    public CreateVehicle setMake(String make) {
        this.make = make;

        return this;
    }

    public String getModel() {
        return model;
    }

    public CreateVehicle setModel(String model) {
        this.model = model;

        return this;
    }

    public String getModelYear() {
        return modelYear;
    }

    public CreateVehicle setModelYear(String modelYear) {
        this.modelYear = modelYear;

        return this;
    }
}
