package com.usedcarlot.usedcarlot.core.vehicles.commands;

public class UpdateVehicle {
    private String make;

    private String model;

    private String modelYear;

    public String getMake() {
        return make;
    }

    public UpdateVehicle setMake(String make) {
        this.make = make;

        return this;
    }

    public String getModel() {
        return model;
    }

    public UpdateVehicle setModel(String model) {
        this.model = model;

        return this;
    }

    public String getModelYear() {
        return modelYear;
    }

    public UpdateVehicle setModelYear(String modelYear) {
        this.modelYear = modelYear;

        return this;
    }
}
