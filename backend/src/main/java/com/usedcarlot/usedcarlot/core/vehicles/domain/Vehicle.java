package com.usedcarlot.usedcarlot.core.vehicles.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Vehicle {
    @Id
    @GeneratedValue
    private Long id;

    private String make;

    private String model;

    private String modelYear;

    public Vehicle() {}

    public Vehicle(String make, String model, String modelYear) {
        this.make = make;
        this.model = model;
        this.modelYear = modelYear;
    }

    public Long getId() {
        return id;
    }

    public Vehicle setId(Long id) {
        this.id = id;

        return this;
    }

    public String getMake() {
        return make;
    }

    public Vehicle setMake(String make) {
        this.make = make;

        return this;
    }

    public String getModel() {
        return model;
    }

    public Vehicle setModel(String model) {
        this.model = model;

        return this;
    }

    public String getModelYear() {
        return modelYear;
    }

    public Vehicle setModelYear(String modelYear) {
        this.modelYear = modelYear;

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(getId(), vehicle.getId()) &&
                Objects.equals(getMake(), vehicle.getMake()) &&
                Objects.equals(getModel(), vehicle.getModel()) &&
                Objects.equals(getModelYear(), vehicle.getModelYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getMake(), getModel(), getModelYear());
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", modelYear='" + modelYear + '\'' +
                '}';
    }
}
