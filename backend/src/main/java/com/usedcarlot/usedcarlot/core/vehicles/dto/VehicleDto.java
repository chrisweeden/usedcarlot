package com.usedcarlot.usedcarlot.core.vehicles.dto;

import com.usedcarlot.usedcarlot.core.vehicles.domain.Vehicle;

public class VehicleDto {
    public static VehicleDto from(final Vehicle vehicle) {
        return new VehicleDto(
            vehicle.getId(),
            vehicle.getMake(),
            vehicle.getModel(),
            vehicle.getModelYear()
        );
    }

    private final Long id;
    private final String make;
    private final String model;
    private final String modelYear;

    public VehicleDto() {
        this(null, "", "", "");
    }

    public VehicleDto(final Long id, final String make, final String model, final String modelYear) {
        this.id = id;
        this.make = make;
        this.model = model;
        this.modelYear = modelYear;
    }

    public Long getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getModelYear() {
        return modelYear;
    }
}
